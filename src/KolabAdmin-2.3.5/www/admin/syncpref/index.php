<?php
/*-
 *  Copyright (c) 2004 Steffen Hansen <steffen@klaralvdalens-datakonsult.se>
 *  Copyright (c) 2011 Bogomil Shopov <shopov@kolabsys.com>
 *  Copyright (c) 2010 Hendrik Helwich <h.helwich@tarent.de>
 *  Copyright (c) 2016 mirabilos <t.glaser@tarent.de>
 *
 *  This  program is free  software; you can redistribute  it and/or
 *  modify it  under the terms of the GNU  General Public License as
 *  published by the  Free Software Foundation; either version 2, or
 *  (at your option) any later version.
 *
 *  This program is  distributed in the hope that it will be useful,
 *  but WITHOUT  ANY WARRANTY; without even the  implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  General Public License for more details.
 *
 *  You can view the  GNU General Public License, online, at the GNU
 *  Project's homepage; see <http://www.gnu.org/licenses/gpl.html>.
 */

require_once('KolabAdmin/include/mysmarty.php');
require_once('KolabAdmin/include/headers.php');
require_once('KolabAdmin/include/locale.php');
require_once('KolabAdmin/include/authenticate.php');
require_once('KolabAdmin/include/form.class.php');
require_once('KolabAdmin/include/imapannotatemore.class.php');

$valid_kolab_annotations = array(
	'contact',
	'contact.default',
	'event',
	'event.default',
	'note',
	'note.default',
	'task',
	'task.default',
    );
mb_internal_encoding('UTF-8');
$debug = KolabForm::getRequestVar('debug');

$errors = array();
$messages = array();
$sidx = 'syncpref';
require_once('KolabAdmin/include/menu.php');
$menuitems[$sidx]['selected'] = 'selected';

/* extract mailboxen from IMAP */

$kolab_imap = new IMAP_annotatemore('127.0.0.1', '993');
$kolab_imap->connect($auth->uid(), $auth->password(), true, false);
try {
	$mb = $kolab_imap->getMailboxes();
} catch (Exception $e) {
	array_push($errors, _($e->getMessage()));
}

/* set folder annotations from POST request */

if (empty($errors) && array_key_exists('submitsyncprefs', $_REQUEST)) {
	/* decode POST values as folder names, cf. below */
	$postvalues = array();
	foreach ($_POST as $key => $value)
		$postvalues[rawurldecode($key)] = $value;

	/* set or delete annotations */
	foreach ($mb as $mailbox) try {
		if (array_key_exists($mailbox, $postvalues))
			$kolab_imap->setAnnotation($mailbox,
			    '/vendor/tarent/syncpref', 'value.priv', 'syncme');
		else {
			$ann_kolab = $kolab_imap->getAnnotation($mailbox,
			    '/vendor/kolab/folder-type', 'value.shared');
			/* only set syncpref annotation for valid folders */
			if (!in_array(trim($ann_kolab), $valid_kolab_annotations))
				continue;
			$ann_ws = $kolab_imap->getAnnotation($mailbox,
			    '/vendor/tarent/syncpref', 'value.priv');
			if ($ann_ws != 'NIL')
				$kolab_imap->setAnnotation($mailbox,
				    '/vendor/tarent/syncpref',
				    'value.priv', 'NIL');
		}
	} catch (Exception $e) {
		array_push($errors, _($e->getMessage()));
	}
	if (empty($errors))
		array_push($messages, _('Update successful'));
}

$imap_folders = array();
foreach ($mb as $mailbox) {
	$entry = array('ok' => false);
	$thiserr = false;

	/*
	 * (raw)urlencode will encode all special chars, including space,
	 * but excluding ‘-’, ‘.’, and ‘_’. PHP calls it a feature and
	 * converts spaces and dots to underscores in POST values (no
	 * kidding!), we’ll have to convert these as well; to make the
	 * output look consistent, we also convert the hyphen-minus.
	 */
	$entry['folder_id'] = str_replace('-', '%2D',
	    str_replace('_', '%5F', str_replace('.', '%2E',
	    rawurlencode($mailbox))));

	/* convert IMAP UTF-7 to UTF-8 for browser display */
	$entry['full_name'] = mb_convert_encoding($mailbox, 'UTF-8', 'UTF7-IMAP');

	try {
		$ann_kolab = $kolab_imap->getAnnotation($mailbox,
		    '/vendor/kolab/folder-type', 'value.shared');
	} catch (Exception $e) {
		$ann_kolab = _($e->getMessage());
		array_push($errors, $ann_kolab);
		if (!$debug)
			continue;
		$thiserr = true;
	}
	if (!$debug && !in_array(trim($ann_kolab), $valid_kolab_annotations))
		continue;
	$entry['ann_kolab'] = $ann_kolab;

	try {
		$ann_ws = $kolab_imap->getAnnotation($mailbox,
		    '/vendor/tarent/syncpref', 'value.priv');
	} catch (Exception $e) {
		$ann_ws = _($e->getMessage());
		array_push($errors, $ann_ws);
		if (!$debug)
			continue;
		$thiserr = true;
	}
	$entry['ann_ws'] = $ann_ws;

	if (!$thiserr && in_array(trim($ann_kolab), $valid_kolab_annotations)) {
		$entry['enabled'] = ($ann_ws === 'syncme');
		$entry['ok'] = true;
	}

	array_push($imap_folders, $entry);
}

/**** Insert into template and output ***/
$smarty = new MySmarty();
//add the plugin
$smarty->plugins_dir[] = 'KolabAdmin/include/';
$smarty->assign( 'errors', $errors );
$smarty->assign( 'messages', $messages );
$smarty->assign( 'uid', $auth->uid() );
$smarty->assign( 'group', $auth->group() );
$smarty->assign( 'page_title', $menuitems[$sidx]['title'] );
$smarty->assign( 'imap_folders', $imap_folders );
$smarty->assign( 'menuitems', $menuitems );
$smarty->assign( 'submenuitems',
				 array_key_exists('submenu',
								  $menuitems[$sidx])?$menuitems[$sidx]['submenu']:array() );
$smarty->assign( 'maincontent', 'syncpref.tpl' );
$smarty->display('page.tpl');
