{* Smarty Template *}
<div class="align_center">
 <a href="http://www.klaralvdalens-datakonsult.se/">
  <img src="../images/kdab.png" alt="KDAB" longdesc="http://www.klaralvdalens-datakonsult.se" style="border:0px;" />
 </a>
</div>
<div class="contentsimple">
<p>
{tr msg="The Kolab 1 KDE client and the Kolab 2 KDE Client (Kontact) was developed by"} <a href="http://www.klaralvdalens-datakonsult.se">Klar&auml;lvdalens Datakonsult AB</a>.
</p>
<p>
{tr msg="Klar&auml;lvdalens Datakonsult AB is a consulting company dedicated to opensource software, Linux, and the <a href=\"http://www.trolltech.com/products/qt/\">Qt</a> library. The main tasks of KDAB within the Kolab project are design and implementation of the Kolab KDE clients and the implementation of the current Kolab 2 server."}
</p>
<p>
{tr msg="The following people worked on Kolab for Klar&auml;lvdalens Datakonsult AB:"}
</p>
<p>
Bo Thorsen<br />
Michel Boyer de la Giroday<br />
Steffen Hansen<br />
Matthias Kalle Dalheimer<br />
David Faure<br />
Karl-Heinz Zimmer<br />
Lutz Rogowski<br />
Romain Pokrzywka<br />
</p>
</div>
