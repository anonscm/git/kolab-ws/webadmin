{* Smarty Template *}
{*
  Local variables:
  buffer-file-coding-system: utf-8
  End:
*}
<div class="contenttext">
<h1>{tr msg="Kolab Groupware Solution"}</h1>
<p>
{tr msg="This is the server of the Kolab Groupware Solution, a <a href=\"http://fsfe.org/about/basics/freesoftware\">Free Software</a> Personal Information Management (PIM) solution developed by the <a href=\"http://www.kolab.org/\">Kolab Community</a>. Any installation of the Kolab Groupware Solution would normally include one or more of these servers, in combination with a variety of clients for its users. If you are interested in the technology, you can read more about it <a href=\"/admin/kolab/technology.php\">here</a>."}
</p>

<h2>{tr msg="Why Kolab?"}</h2>

<p>{tr msg="There is a wide variety of reasons why Kolab should be your choice. Several of them have been put together for different target groups in the"} <a href="http://files.kolabsys.com/Public/Handouts/">{tr msg="Handouts for the Kolab Groupware Solution by Kolab Systems"}</a>. {tr msg="To summarize just some points, Kolab is your solution, if"}</p>

<ul>
 <li>{tr msg="you want a solution that seamlessly integrates into your existing environment;"}</li>
 <li>{tr msg="you want full control over your data, with all the convenience of the cloud;"}</li>
 <li>{tr msg="you have elevated security needs for your data;"}</li>
 <li>{tr msg="you have a very large installation, or want to reduce your hardware footprint;"}</li>
 <li>{tr msg="you have to operate in diverse environments and installations;"}</li>
 <li>{tr msg="strategic issues are important to you, e.g. you need sovereignty of the software you deploy;"}</li>
 <li>{tr msg="your users should be fully productive whether they are on- or offline;"}</li>
 <li>{tr msg="you want the flexibility to create solutions for your specialised needs;"}</li>
 <li>{tr msg="you want Free Software/Open Source, but also want the professional support without having to resort to proprietary lock-in."}</li>
</ul>

<h2>{tr msg="More Information"}</h2>
<ul>
 <li><a href="http://www.kolab.org">{tr msg="Kolab Community Web Site"}</a></li>
 <li><a href="http://wiki.kolab.org">{tr msg="Kolab Community Wiki"}</a></li>
 <li><a href="http://blogs.fsfe.org/greve/?p=431">{tr msg="Article: "} 'The Kolab Story' by Georg Greve</a></li>
 <li><a href="http://files.kolabsys.com/Public/Handouts/">{tr msg="Handouts for the Kolab Groupware Solution by Kolab Systems"}</a></li>
</ul>

<h2>{tr msg="Warranties &amp; Commercial Support"}</h2>

<p>
{tr msg="This is the <b>Community Edition</b> of the Kolab Groupware Server. It is provided by Kolab Systems as fully <a href=\"http://fsfe.org/about/basics/freesoftware\">Free Software</a> and you can use it to the fullest extent and any purpose. There are no license costs or other obligations other than those set forth in the respective Free Software licenses of the various components. <b>There are no warranties or guarantees of any kind, nor is there any guaranteed support. You are running this software entirely at your own risk.</b>"}
</p>

<p>
{tr msg="There is a wide variety of commercial support options and warranties available by <a href=\"/admin/kolab/kolabsystems.php\">Kolab Systems AG</a> and its network of partners, the Kolab Enterprise Community. This support comes with access to the additionally quality assured and hardened <b>Certified Kolab</b> edition, which like the community edition is fully available as Free Software, so has all the advantages of the Community Edition and is free of hidden lock-in."}</p>

<p>{tr msg="Kolab Systems AG will provide limited support for the Community Edition on a case-by-case basis, but with no guaranteed response times, as availability permits, and at rates that may be adjusted due to the work load at the time of the request. <b>It is strongly recommended that for mission-critical professional deployments of the Kolab Groupware Solution you at least choose one of the partially self-supported On Demand support models.</b>"}
</p>

<p>{tr msg="<center><table cellspacing=\"2\" cellpadding=\"2\" border=\"1\">
<tr><td></td><td><b>This edition</b></td><td><em>With SLA by Kolab Systems</em></b></td></tr>
<tr><td>Support channels</td><td><b>Wiki &amp; public mailing lists only</b></td><td><em>Kolab Systems Ticketing</em></td></tr>
<tr><td>Guaranteed response times</td><td><b>NO</b></td><td><em>YES</em></td></tr>
<tr><td>Access to Certified Kolab</td><td><b>NO</b></td><td><em>YES</em></td></tr>
<tr><td>Warranties</td><td><b>NO</b></td><td><em>YES</em></td></tr>
<tr><td>Access to custom versions</td><td><b>NO</b></td><td><em>YES</em></td></tr>
</table></center>"}</p>

<p>{tr msg="Kolab Systems provides an overview of all available <a href=\"http://kolabsys.com/index.php/solutions/101\">Service Level Agreements (SLA)</a>. To upgrade your installation to Certified Kolab, please contact"} <a href="getkolab@kolabsys.com">getkolab@kolabsys.com</a> {tr msg="or visit our"} <a href="http://kolabsys.com/index.php/component/form/Get%20Kolab?form_id=2">{tr msg="web page"}</a>.</p>


<hr>
<small>
  <p>
    This product includes PHP, freely available from
    http://www.php.net/
  </p><p>
    This product includes software developed by the OpenSSL Project
    for use in the OpenSSL Toolkit (http://www.openssl.org/)
  </p><p>
    This product is patched for use with kolab-ws by <span
    style="color:#CC0000; white-space:nowrap;">⮡ tarent</span>
  </p>
</small>

</div>
