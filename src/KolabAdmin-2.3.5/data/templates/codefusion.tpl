{* Smarty Template *}
{*
 (c) 2004 Code Fusion cc
 This program is Free Software under the GNU General Public License (>=v2).
 Read the file COPYING that comes with this packages for details.
*}
<div class="contentsimple">

<a href="http://www.codefusion.co.za/">
 <img src="../images/codefusion.png" alt="Code Fusion" style="border:0" />
</a>
  
<p>
{tr msg="<a href=\"http://www.codefusion.co.za/\">Code Fusion cc</a> is a specialist email solution provider based in Johannesburg, South Africa specialising in the deployment and support of Kolab."}
</p>

<p>
{tr msg="Code Fusion has played, and continues to play, an integral part in developing and extending the Kolab server; specifically with regards to enhancing the base Kolab engine, adding in support for Microsoft Active Directory &reg; as an LDAP backend, as well as extending the <a href=\"http://www.horde.org/\">Horde project</a> to provide a web-based groupware client for the Kolab server."}
</p>

<p>
{tr msg="The following people from Code Fusion are involved in the Kolab project (in alphabetical order):"}
</p>

<p>
Stephan Buys<br />
</p>

</div>
