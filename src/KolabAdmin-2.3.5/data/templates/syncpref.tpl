{* Smarty Template *}
{*
  Local variables:
  buffer-file-coding-system: utf-8
  End:
*}
<div class="contenttext">
<h3>{tr msg="Synchronisation Preferences for kolab-ws"}</h3>
</div>

<div class="contentform">
<form id="syncprefform" method="post" action="">
<div>
<table cellpadding="0" cellspacing="1px">
<tr class="contentrow">
	<th style="white-space:nowrap; width:1em; text-align:center;">{tr msg="✓"}</th>
	<th style="white-space:nowrap;">{tr msg="IMAP folder"}</th>
	<th style="white-space:nowrap;">{tr msg="Kolab annotation"}</th>
	<th style="white-space:nowrap;">{tr msg="WS annotation"}</th>
</tr>
{section name=id loop=$imap_folders}
<tr class="contentrow{cycle values="even,odd"}">
	<td class="actioncell">{if $imap_folders[id].ok}<input type="checkbox"
	 {if $imap_folders[id].enabled}checked="checked"{/if}
	 name="{$imap_folders[id].folder_id}" />{/if}</td>
	<td class="contentcell">{$imap_folders[id].full_name|escape:"html"}</td>
	<td class="contentcell">{if $imap_folders[id].ann_kolab == 'NIL'}<i
	 style="color:#999999;">{tr msg="unset"}</i>{else}{$imap_folders[id].ann_kolab|escape:"html"}{/if}</td>
	<td class="contentcell">{if $imap_folders[id].ann_ws == 'NIL'}<i
	 style="color:#999999;">{tr msg="unset"}</i>{else}{$imap_folders[id].ann_ws|escape:"html"}{/if}</td>
</tr>
{/section}
</table>
<div class="align_right"><input type="submit" name="submitsyncprefs" value="{tr msg="Update"}" /></div>
</div>
</form>
</div>

<hr />
<p>{tr msg="kolab-ws and SimKolab are products of <a href=\"https://www.tarent.de/\" style=\"text-decoration:none\"><span style=\"color:#CC0000; white-space:nowrap;\">⮡ tarent</span></a> and not affiliated with Kolab Systems."}</p>
