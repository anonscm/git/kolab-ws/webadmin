{* Smarty Template *}
{*
 (c) 2003 Tassilo Erlewein <tassilo.erlewein@erfrakon.de>
 (c) 2004-2006 Martin Konold <martin.konold@erfrakon.de>
 This program is Free Software under the GNU General Public License (>=v2).
 Read the file COPYING that comes with this packages for details.
*}
<div class="contentsimple">
<p>
{tr msg="Kolab's architecture was done by <a href=\"http://www.erfrakon.com\">erfrakon</a>; the company also designed and implemented the Kolab 1 Server and did the design and architecture for the Kolab 2 Server while providing consulting for the implementation of the Kolab 2 server and the Kolab 2 clients."}
</p>
<div class="align_center">
 <a href="http://www.erfrakon.de">
  <img src="../images/erfrakon.png" alt="erfrakon" longdesc="http://www.erfrakon.de" usemap="#maperfrakon" style="border:0px;" />
 </a>
</div>
<p>
{tr msg="<a href=\"http://www.erfrakon.com\">erfrakon</a> is a consulting company dedicated to opensource software and Linux. The main tasks of erfrakon within the Kolab project are the principle design and architecure of the Kolab groupware solution and the creation of the Kolab 1 server."}
</p>
<p>
{tr msg="The following people worked on Kolab for erfrakon:"}
</p>
<p>
Tassilo Erlewein<br />
Achim Frank<br />
Martin Konold<br />
</p>
</div>
