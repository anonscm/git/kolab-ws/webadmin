{* Smarty Template *}
<div class="contentsimple">
<p>
{tr msg="Intevation GmbH coordinated the Projects: Kroupware and Proko2, which are the main driving force behind Kolab1&2. In addition to project management Intevation did most of the quality assurance."}
</p>

<div class="align_center">
 <a href="http://intevation.net/">
  <img src="../images/intevation_logo.png" alt="Logo Intevation GmbH" height="91" longdesc="www.intevation.net" style="border:0px;" />
 </a>
</div>

<p>
{tr msg="Intevation GmbH is a IT-company exclusively focusing on Free Software. Its business units are strategic consulting, project management and geographic information systems."}
</p>
<p>
{tr msg="The following people worked on Kolab for Intevation:"}
</p>
<p>
Bernhard Reiter<br />
Jan-Oliver Wagner<br />
Marc Mutz
</p>
</div>
