{* Smarty Template *}
<div class="align_center">
 <a href="http://kolabsys.com/">
  <img src="/admin/images/kolabsystems_logo.png" alt="Kolab Systems AG" longdesc="http://kolabsys.com" style="border:0px;" />
 </a>
</div>
<div class="contentsimple">
<p>
{tr msg="Founded in Z&uuml;rich in 2010, "}<a href="http://kolabsys.com">Kolab Systems AG</a> {tr msg="is the global leader in Kolab Technology. Together with its partners in the Kolab Enterprise Community, Kolab Systems provides quality assurance, integration, support, training, certification of individuals and third-party products, as well as ongoing development for the Kolab Groupware Solution. The strong partnering model and a highly competent enterprise community provides users of the Kolab Groupware Solution with the right level of professional consulting, support and warranties to enable productivity and collaboration."}
</p>

<h2>{tr msg="Looking for professional support for your Kolab Groupware Solution?"}</h2>

<p>{tr msg="Kolab Systems provides a wide range of "} <a href="http://kolabsys.com/index.php/solutions/101">Service Level Agreements (SLA)</a> {tr msg= "which range from fully self-supported installations to maintained installations with reaction times as low as 1 hour. These are provided exclusively on Kolab Systems' Certified Kolab edition. To upgrade your installation to Certified Kolab, please contact"} <a href="getkolab@kolabsys.com">getkolab@kolabsys.com</a> {tr msg="or visit our"} <a href="http://kolabsys.com/index.php/component/form/Get%20Kolab?form_id=2">{tr msg="web page"}</a>.
</p>

<h2>{tr msg="Our Development Partners"}</h2>

<p>{tr msg="In continuously improvingall parts of the Kolab Groupware Solution, Kolab Systems is working closely with development partners, such as (in alphabetical order):"}</p>

<ul>
<li><a href="http://www.pardus.de">P@rdus</a></li>
<li><a href="/admin/kolab/intevation.php">Intevation</a></li>
<li><a href="/admin/kolab/kdab.php">KDAB</a></li>
<li><a href="http://www.libertech.fr/default.htm">Libertech</a></li>
<li>...and all the community contributors.
</ul>

<p><b>{tr msg="Thank you for being part of this awesome community!"}</b></p>

<p>{tr msg="If <b>you</b> want to join the community, please go to "} <a href="http://kolab.org">kolab.org</a> {tr msg="and"} <a href="http://wiki.kolab.org">wiki.kolab.org</a>.

</div>
