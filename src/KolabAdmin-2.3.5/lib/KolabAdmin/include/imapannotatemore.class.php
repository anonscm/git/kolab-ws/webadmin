<?php

/*-
 * Copyright © 2016
 *	mirabilos <t.glaser@tarent.de>
 * Copyright © 2009
 *	Axel Dirla <axel.dirla@tarent.de>
 *
 * Provided that these terms and disclaimer and all copyright notices
 * are retained or reproduced in an accompanying document, permission
 * is granted to deal in this work without restriction, including un‐
 * limited rights to use, publicly perform, distribute, sell, modify,
 * merge, give away, or sublicence.
 *
 * This work is provided “AS IS” and WITHOUT WARRANTY of any kind, to
 * the utmost extent permitted by applicable law, neither express nor
 * implied; without malicious intent or gross negligence. In no event
 * may a licensor, author or contributor be held liable for indirect,
 * direct, other damage, loss, or other issues arising in any way out
 * of dealing in the work, even if advised of the possibility of such
 * damage or existence of a defect, except proven that it results out
 * of said person’s immediate fault when using the work as intended.
 */

class IMAP_annotatemore {
	/**
	 * annotation cache to reduce amount of queries
	 *
	 * @var array $_annotation_cache
	 */
	var $_annotation_cache;

	/**
	 * IMAP server hostname
	 *
	 * @var string $_server
	 */
	var $_server;

	/**
	 * IMAP server port
	 *
	 * @var string $_port
	 */
	var $_port;

	/**
	 * IMAP username
	 *
	 * @var string $_login
	 */
	var $_login;

	/**
	 * IMAP password
	 *
	 * @var string $_password;
	 */
	var $_password;

	/**
	 * Basic IMAP connection string
	 *
	 * @var string $_base_mbox
	 */
	var $_base_mbox;

	/**
	 * IMAP connection string with folder
	 *
	 * @var string $_mbox
	 */
	var $_mbox;

	/**
	 * Signature of the current connection
	 *
	 * @var string $_signature
	 */
	var $_signature;

	/**
	 * Copy of the current signature
	 *
	 * @var string $_reuse_detection
	 */
	var $_reuse_detection;

	/**
	 * Constructor; sets server hostname and port
	 *
	 * @param string $server	hostname (default 'localhost')
	 * @param string $port		port number (default '143')
	 */
	function __construct($server='localhost', $port='143') {
		$this->_server = $server;
		$this->_port = $port;
	}

	/**
	 * Connects to the IMAP server
	 *
	 * @param string $login		username
	 * @param string $password	password
	 * @param bool $tls		use TLS? (default false)
	 * @param bool $cert		validate certificate? (default true)
	 */
	function connect($login, $password, $tls=false, $cert=true) {
		$mbox = '{' . $this->_server . ':' . $this->_port;
		if ($tls) {
			$mbox .= '/imap/ssl';
			if (!$cert)
				$mbox .= '/novalidate-cert';
		} else
			$mbox .= '/notls';
		$mbox .= '}';

		$this->_signature = "$mbox|$login|$password";
		if ($this->_signature === $this->_reuse_detection)
			return;

		$this->_mbox = $this->_base_mbox = $mbox;
		$this->_login = $login;
		$this->_password = $password;
		$this->_imap = null;
		/* connection is lazy */

		$this->_reuse_detection = $this->_signature;
	}

	/**
	 * Lazily connects to the IMAP server; disconnection is
	 * handled by finishing to serve the page instead
	 */
	function _connect() {
		if (!($r = @imap_open($this->_base_mbox, $this->_login,
		    $this->_password, OP_HALFOPEN)))
			throw new Exception(sprintf(_('IMAP error while connecting to "%s": %s'),
			    $this->_server, @imap_last_error()));
		$this->_imap = $r;
	}


	/**
	 * Retrieves a list of mailboxes from the server
	 *
	 * @return string[] list of mailboxes
	 */
	function getMailboxes() {
		if (!isset($this->_imap))
			$this->_connect();

		$folders = array();
		if (!($r = @imap_list($this->_imap, $this->_base_mbox, '*')))
			throw new Exception(sprintf(_('IMAP error while retrieving list of mailboxes on "%s": %s'),
			    $this->_base_mbox, @imap_last_error()));
		$len = strlen($this->_base_mbox);
		foreach ($r as $folder)
			if (substr($folder, 0, $len) === $this->_base_mbox)
				$folders[] = substr($folder, $len);
		return $folders;
	}

	/**
	 * Fetches a folder annotation
	 *
	 * @param string $mbox		folder name
	 * @param string $entry		entry to fetch
	 * @param string $key		attribute to fetch
	 * @return mixed annotation value or 'NIL'
	 */
	function getAnnotation($mbox, $entry, $key) {
		if (!isset($this->_imap))
			$this->_connect();

		$signature = "$this->_signature|$entry|$key|$mbox";
		if (isset($this->_annotation_cache[$signature]))
			return $this->_annotation_cache[$signature];

		if (($r = @imap_getannotation($this->_imap, $mbox, $entry,
		    $key)) === false)
			throw new Exception(sprintf(_('IMAP error while retrieving annotation %s, attribute %s, for folder %s: %s'),
			    $entry, $key, $mbox, @imap_last_error()));
		$this->_annotation_cache[$signature] = isset($r[$key]) ?
		    $r[$key] : 'NIL';
		return $this->_annotation_cache[$signature];
	}

	/**
	 * Sets or deletes a folder annotation
	 *
	 * @param string $mbox		folder name
	 * @param string $entry		entry to set
	 * @param string $key		attribute to set
	 * @param string $value		values to set, 'NIL' to delete
	 */
	function setAnnotation($mbox, $entry, $key, $value) {
		if (!isset($this->_imap))
			$this->_connect();

		if (!@imap_setannotation($this->_imap, $mbox, $entry,
		    $key, $value))
			throw new Exception(sprintf(_('IMAP error while setting annotation %s, attribute %s, for folder %s, to %s: %s'),
			    $entry, $key, $mbox, $value, @imap_last_error()));
		$signature = "$this->_signature|$entry|$key|$mbox";
		$this->_annotation_cache[$signature] =
		    strtoupper(trim($value)) === 'NIL' ? 'NIL' : $value;
	}
}
