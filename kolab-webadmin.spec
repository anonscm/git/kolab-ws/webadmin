# Source code for the kolab-webadmin component for Kolab 2.3.5
# patched to allow setting kolab-ws-specific synchronisation
# preferences; also covers additional customer-requested changes:
# - FORCED_VACATION_REPLY_DOMAIN in webadmin.conf.template
# - hide Kolab’s own Z-Push® settings menu

# Variables
#%define         V_pear_pkgdir 
%define         V_pear_package KolabAdmin
%define         V_package kolab-webadmin
%define         V_package_url http://pear.horde.org/index.php?package=
%define         V_package_origin WGET
#%define         V_repo_commit 
#%define         V_repo_release 
%define         V_version 2.3.5
%define         V_release 20141211
%define         V_tarent 14
%define         V_sourceurl http://files.kolab.org/server/development-2.3/sources
%define         V_php_lib_loc php
%define         V_summary A frontend for the Kolab user database.
%define         V_license GPLv2-or-later & LGPLv2.1-unspecified & BSD-3clause & MIT

# Package Information
Name:	   %{V_package}
Summary:   %{V_summary}
URL:       {%V_package_url}%{V_pear_package}
Packager:  Gunnar Wrobel <wrobel@pardus.de> (p@rdus)
Version:   %{V_version}
Release:   %{V_release}tarent%{V_tarent}
License:   %{V_license}
Group:     Development/Libraries
Distribution:	OpenPKG

# List of Sources
Source0:    %{V_sourceurl}/%{V_pear_package}-%{V_version}.tgz
Source1:    webadmin.conf.template

# List of patches
Patch0:    package.patch
Patch1:    tarent.patch

# Build Info
Prefix:	   %{l_prefix}
BuildRoot: %{l_buildroot}

#Pre requisites
                              
BuildPreReq:  OpenPKG, openpkg >= 20070603 
BuildPreReq:  php, php::with_pear = yes    
BuildPreReq:  PEAR-Horde-Channel           

                                   
PreReq:       OpenPKG, openpkg >= 20070603 
PreReq:       php, php::with_pear = yes    
PreReq:       php-smarty >= 2.6.20

# Package options
%option       with_chroot              no

%description 
 
This package provides a web frontend for editing the
Kolab user database stored in LDAP.


%prep
	%setup -n %{V_pear_package}-%{V_version}

        if [ -n "`cat %{PATCH0}`" ]; then
	    %patch -p3 -P 0
	fi

	cd ..
	%patch -p1 -P 1
	cd -
	sh data/locale/format_messages

	cat ../package.xml | sed -e 's/md5sum="[^"]*"//' > package.xml

%build

%install
        %{l_shtool} install -d $RPM_BUILD_ROOT%{l_prefix}/var/kolab/php/admin/templates_c
	%{l_shtool} install -d $RPM_BUILD_ROOT%{l_prefix}/etc/kolab/templates	

	env PHP_PEAR_PHP_BIN="%{l_prefix}/bin/php -d safe_mode=off -d memory_limit=40M"\
            PHP_PEAR_CACHE_DIR="/tmp/pear/cache"                                       \
	    %{l_prefix}/bin/pear -d www_dir="%{l_prefix}/var/kolab/www"   \
	                         -d php_dir="%{l_prefix}/lib/%{V_php_lib_loc}"         \
                                 install --offline --force --nodeps -P $RPM_BUILD_ROOT \
                                 package.xml

	rm -rf $RPM_BUILD_ROOT/%{l_prefix}/lib/%{V_php_lib_loc}/{.filemap,.lock,.channels,.depdb,.depdblock}

        # With chroot
        %if "%{with_chroot}" == "yes"
                %{l_shtool} mkdir -f -p -m 755 $RPM_BUILD_ROOT%{l_prefix}/var/kolab/www/%{l_prefix}/lib
                cp -a $RPM_BUILD_ROOT/%{l_prefix}/lib/%{V_php_lib_loc} $RPM_BUILD_ROOT%{l_prefix}/var/kolab/www/%{l_prefix}/lib/
        %endif

	%{l_shtool} install -c -m 644 %{l_value -s -a} %{S:1} \
	  $RPM_BUILD_ROOT%{l_prefix}/etc/kolab/templates

        %{l_rpmtool} files -v -ofiles -r$RPM_BUILD_ROOT %{l_files_std}                     \
            %dir '%defattr(-,%{l_nusr},%{l_ngrp})' %{l_prefix}/var/kolab/php/admin/templates_c


%clean
	rm -rf $RPM_BUILD_ROOT

%files -f files

%changelog
* Wed Jul 05 2017 mirabilos <t.glaser@tarent.de> - 2.3.5-20141112tarent14
- update to current ⮡ tarent build infrastructure

* Mon Apr 25 2016 mirabilos <t.glaser@tarent.de> - 2.3.5-20141112tarent12
- replace ActiveSync with SyncPref in welcome screen for user login

* Wed Apr 20 2016 mirabilos <t.glaser@tarent.de> - 2.3.5-20141112tarent11
- customer-specific FORCED_VACATION_REPLY_DOMAIN in webadmin.conf.template
  (can be defined to a domain, or empty, or undefined to allow editing)
  ⚠ this is set by default; change this to suit your needs ⚠
- hide Kolab’s own ActiveSync settings in favour of using SimKolab

* Wed Apr 20 2016 mirabilos <t.glaser@tarent.de> - 2.3.5-20141112tarent9
- debug mode for the syncpref form
- use SSL for imapd connection, even locally (avoids PLAIN warning)
- pass NIL magic value through the stack

* Tue Apr 19 2016 mirabilos <t.glaser@tarent.de> - 2.3.5-20141112tarent6
- adapt for tarent / kolab-ws
- drop sliding-tabs.js (no explicit licence; only used in Kolab’s own
  ActiveSync which we don’t use as we ship SimKolab instead)
- add page to set synchronisation preferences for kolab-ws

* Wed Nov 28 2014 Sascha Wilde <wilde@intevation.de> - 2.3.5-20141112
- Update to 2.3.5

* Tue Sep 28 2011 Christoph Wickert <wickert@kolabsys.com> - 2.3.4-20110927
- Update to 2.3.4 (fixes bugzilla.kolabsys.com #442)

* Tue Sep 13 2011 Christoph Wickert <wickert@kolabsys.com> - 2.3.3-20110913
- Update to 2.3.3 (fixes kolab/issue4777 and kolab/issue4779)

* Fri Jun 03 2011 Christoph Wickert <wickert@kolabsys.com> - 2.3.2-20110603
- Update to 2.3.2

* Thu Apr 28 2011 Christoph Wickert <wickert@kolabsys.com> - 2.3.1-20110428
- Update to 2.3.1

* Thu Apr 14 2011 Christoph Wickert <wickert@kolabsys.com> - 2.3.0-20110414
- Update to 2.3.0
