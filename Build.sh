#!/bin/mksh
#-
# Copyright © 2016, 2017, 2018
#	mirabilos <t.glaser@tarent.de>
#
# Provided that these terms and disclaimer and all copyright notices
# are retained or reproduced in an accompanying document, permission
# is granted to deal in this work without restriction, including un‐
# limited rights to use, publicly perform, distribute, sell, modify,
# merge, give away, or sublicence.
#
# This work is provided “AS IS” and WITHOUT WARRANTY of any kind, to
# the utmost extent permitted by applicable law, neither express nor
# implied; without malicious intent or gross negligence. In no event
# may a licensor, author or contributor be held liable for indirect,
# direct, other damage, loss, or other issues arising in any way out
# of dealing in the work, even if advised of the possibility of such
# damage or existence of a defect, except proven that it results out
# of said person’s immediate fault when using the work as intended.

set -A rcp -- rsync -zavPH --numeric-ids -S --stats '--rsh=ssh -T'
dsthost=bsikolab.tarent.de
dstuser=root
dstgrp=0
set -A v -- $(sed -n -e '/^%d.*V_version */s///p' \
    -e '/^%d.*V_release */s///p' -e '/^%d.*V_tarent */s///p' kolab-webadmin.spec)
v=${v[0]}-${v[1]}tarent${v[2]}
a=amd64-debian7

do_extract() (
	cd obj || return 1
	tar xzf ../KolabAdmin-2.3.5.tgz
)

do_diff() {
	set +e
	diff -pruN obj src >tarent.patch
	local rv=$?
	set -e
	(( rv == 1 ))
}

do_deploy() (
	cd obj || return 1
	mvn -B deploy
)


set -ex
cd "$(dirname "$0")"
rm -rf obj
mkdir obj
do_extract
do_diff
"${rcp[@]}" kolab-webadmin.spec KolabAdmin-2.3.5.tgz package.patch tarent.patch \
    webadmin.conf.template "$dstuser@$dsthost:/kolab/RPM/SRC/kolab-webadmin/"
ssh -l "$dstuser" "$dsthost" chown -R "${dstuser@Q}:${dstgrp@Q}" \
    '/kolab/RPM/SRC/kolab-webadmin/*'
ssh -l "$dstuser" "$dsthost" /kolab/bin/openpkg rpmbuild -ba \
    /kolab/RPM/SRC/kolab-webadmin/kolab-webadmin.spec
"${rcp[@]}" \
    "$dstuser@$dsthost:/kolab/RPM/PKG/kolab-webadmin-$v.src.rpm" \
    "$dstuser@$dsthost:/kolab/RPM/PKG/kolab-webadmin-$v.$a-kolab.rpm" \
    obj/

cat >obj/assembly.xml <<EOF
<assembly xmlns="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/plugins/maven-assembly-plugin/assembly/1.1.2 http://maven.apache.org/xsd/assembly-1.1.2.xsd">
	<id>rpms</id>
	<formats>
		<format>tgz</format>
	</formats>
	<files>
		<file>
			<source>kolab-webadmin-$v.src.rpm</source>
			<outputDirectory></outputDirectory>
		</file>
		<file>
			<source>kolab-webadmin-$v.$a-kolab.rpm</source>
			<outputDirectory></outputDirectory>
		</file>
	</files>
</assembly>
EOF

cat >obj/pom.xml <<EOF
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<parent>
		<groupId>org.evolvis.tartools</groupId>
		<artifactId>maven-parent</artifactId>
		<version>1.6</version>
	</parent>
	<groupId>org.evolvis.bsi</groupId>
	<artifactId>kolab-webadmin</artifactId>
	<version>$v</version>
	<packaging>pom</packaging>
	<name>${project.groupId}:${project.artifactId}</name>
	<description>BSI Kolab Webadmin</description>
	<url>https://bsi-evolvis.tarent.de/</url>
	<licenses>
		<license>
			<name>GPLv2-or-later</name>
			<url>http://www.gnu.org/licenses/old-licenses/gpl-2.0.html</url>
		</license>
		<license>
			<name>LGPLv2.1-unspecified</name>
			<url>http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html</url>
		</license>
		<license>
			<name>BSD-3clause</name>
			<url>https://opensource.org/licenses/BSD-3-Clause</url>
		</license>
		<license>
			<name>MIT</name>
			<url>https://opensource.org/licenses/MIT</url>
		</license>
	</licenses>
	<developers>
		<developer>
			<name>mirabilos</name>
			<email>t.glaser@tarent.de</email>
			<organization>⮡ tarent</organization>
			<organizationUrl>http://www.tarent.de/</organizationUrl>
		</developer>
	</developers>
	<scm>
		<url>https://evolvis.org/plugins/scmgit/cgi-bin/gitweb.cgi?p=kolab-ws/webadmin.git</url>
		<connection>scm:git:https://evolvis.org/anonscm/git/kolab-ws/webadmin.git</connection>
		<developerConnection>scm:git:ssh://maven@evolvis.org/scmrepos/git/kolab-ws/webadmin.git</developerConnection>
		<tag>HEAD</tag>
	</scm>
	<distributionManagement>
		<repository>
			<id>tarent-nexus</id>
			<name>tarent-nexus releases</name>
			<url>https://repo-bn-01.lan.tarent.de/repository/maven-releases</url>
		</repository>
	</distributionManagement>
	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<executions>
					<execution>
						<id>dist-assembly</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
				<configuration>
					<descriptors>
						<descriptor>assembly.xml</descriptor>
					</descriptors>
					<tarLongFileMode>posix</tarLongFileMode>
				</configuration>
			</plugin>
		</plugins>
	</build>
</project>
EOF

do_deploy

set +x
print -r -- "Success. To install, run: /kolab/bin/openpkg" \
    "rpm -Uhv /kolab/RPM/PKG/kolab-webadmin-$v.$a-kolab.rpm"
print -r -- Distribution: \
    'https://repo-bn-01.lan.tarent.de/#browse/search=attributes.maven2.groupId%3Dorg.evolvis.bsi%20AND%20attributes.maven2.artifactId%3Dkolab-webadmin%20AND%20version%3D'"$v"
exit 0
